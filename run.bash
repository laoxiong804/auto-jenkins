#!/bin/bash

docker stop jenkins

docker rm jenkins

docker run --name jenkins --env JAVA_OPTS="-Duser.timezone=GMT+08" -p 8080:8080 -p 6080:6080 -p 50000:50000 -v /var/run/docker.sock:/var/run/docker.sock  -v $(which docker):/bin/docker -v /lvdata/jenkins_home:/var/jenkins_home -v /usr/lib64/libltdl.so.7:/usr/lib/x86_64-linux-gnu/libltdl.so.7  -d auto-jenkins